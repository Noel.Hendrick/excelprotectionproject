﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class PRAM
    {

        Calculator calculator = new Calculator();

        public string[,] ReturnPRAM(string[,] Dataset, double _selfprobablity)
        {
            for (int i = 0; i < Dataset.GetLength(1); i++)
            {
                Dictionary<string , List<PRAMObject>> TransitionMatrix = CreateTransitionMatrix(Dataset,i, _selfprobablity);
                Dataset = SwapValues(Dataset, TransitionMatrix, i);
            }

            return Dataset;
        }

        public Dictionary<string, List<PRAMObject>> CreateTransitionMatrix(string[,] Dataset, int index, double _selfprobablity)
        {
            Dictionary<string, List<PRAMObject>> TransitionMatrix = new Dictionary<string, List<PRAMObject>>();
            List<string> ListOfVaribles = new List<string>();

            for (int i = 0; i < Dataset.GetLength(0); i++)
            {
                if (ListOfVaribles.Contains(Dataset[i, index]) == false)
                {
                    ListOfVaribles.Add(Dataset[i, index]);
                }
            }

            double selfProbablity;
            double otherProbablity;
            int numberOfVaribles = ListOfVaribles.Count();
            if (numberOfVaribles == 1)
            {
                selfProbablity = 1;
                otherProbablity = 0;
            }
            else
            {
                selfProbablity = _selfprobablity;
                double temp = (double)numberOfVaribles - 1;
                otherProbablity = (1 - selfProbablity) / temp;
            }
            
            

            for (int i =0; i < ListOfVaribles.Count(); i++)
            {
                List<PRAMObject> TransitionRow = new List<PRAMObject>();
                double maxProbablity = selfProbablity;

                for (int j = 0; j < ListOfVaribles.Count(); j++)
                {
                    
                    if(ListOfVaribles[i] == ListOfVaribles[j])
                    {
                        PRAMObject probRange = new PRAMObject(0, selfProbablity, ListOfVaribles[i]);
                        TransitionRow.Add(probRange);
                    }

                    else

                    {

                        PRAMObject probRange = new PRAMObject(maxProbablity, maxProbablity + otherProbablity, ListOfVaribles[j]);
                        TransitionRow.Add(probRange);
                        maxProbablity = maxProbablity + otherProbablity;
                    }
                }

                TransitionMatrix.Add(ListOfVaribles[i], TransitionRow);
            }

            return TransitionMatrix;
        }


        public string[,] SwapValues(string[,] Dataset, Dictionary<string, List<PRAMObject>> TransitionMatrix , int Columnindex)
        {
            Random random = new Random();

            for(int i = 0; i<Dataset.GetLength(0); i++)
            {
                double r = random.NextDouble();
                r = calculator.SetRoundedValue(r);
                string swap = GetValueToSwap(r, Dataset[i, Columnindex], TransitionMatrix);
                int Index = FindEntryToSwap(Dataset, swap, Columnindex);
                Dataset = PreformSwap(Dataset, i, Index, Columnindex);

            }

            return Dataset;
        }

        public int FindEntryToSwap(string[,] Dataset , string ValueToSwap, int index)
        {
            Random random = new Random();
            int randomIndex = 0;
            while(Dataset[randomIndex, index] != ValueToSwap)
            {
                randomIndex = random.Next(0, Dataset.GetLength(0));
            }

            return randomIndex;
        }

        public string[,] PreformSwap(string[,] Dataset, int Row1Index, int Row2Index, int ColumnIndex)
        {
            string temp = Dataset[Row2Index,ColumnIndex];
            Dataset[Row2Index, ColumnIndex] = Dataset[Row1Index, ColumnIndex];
            Dataset[Row1Index, ColumnIndex] = temp;
            return Dataset;

        }

        public string GetValueToSwap(double num, string value, Dictionary<string, List<PRAMObject>> TransitionMatrix)
        {
            for(int i = 0; i<TransitionMatrix.Count(); i++)
            {
                if (TransitionMatrix[value][i].CheckIfInRange(num) == true)
                {
                    return TransitionMatrix[value][i].Value;
                }
  
            }

            return "No Value To Swap";

        }


    }
}
