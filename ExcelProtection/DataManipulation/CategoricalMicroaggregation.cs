﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class CategoricalMicroaggregation
    {
        public int ClusterLimit;
        public CategoricalMicroaggregationObject[] Dataset;
        public Calculator calculator = new Calculator();

        public CategoricalMicroaggregation(string[,] _dataset, int _clusterlimit)
        {
            var enumList1 = Enumerable.Range(0, _dataset.GetLength(0)).Select(o => _dataset[o, 0]);
            HashSet<string> temp2 = enumList1.ToHashSet();

            Dataset = new CategoricalMicroaggregationObject[_dataset.GetLength(0)];
            ClusterLimit = _clusterlimit;

            for (int RowIndex = 0; RowIndex < _dataset.GetLength(0); RowIndex++)
            {
                CategoricalMicroaggregationObject TransferInstance = new CategoricalMicroaggregationObject();
                List<string> tempList = new List<string>();

                for (int ColumnIndex = 0; ColumnIndex < _dataset.GetLength(1); ColumnIndex++)
                {

                    tempList.Add(_dataset[RowIndex, ColumnIndex]);

                }

                TransferInstance.Row = tempList;
                TransferInstance.IsPartOfCluster = false;
                TransferInstance.IndexInOrginalDataset = RowIndex;

                Dataset[RowIndex] = TransferInstance;

            }

            var enumList = Dataset.Select(o => o.Row[0]);
            HashSet<string> temp1 = enumList.ToHashSet();

        }


        public string[,] Microaggregator()
        {
            for (int index = 0; index < Dataset.Length; index++)
            {
                if (Dataset[index].IsPartOfCluster == false)
                {
                    CategoricalMicroaggregationObject CenterOfCluster = Dataset[index];
                    List<CategoricalMicroaggregationObject> Cluster = CreateCluster(CenterOfCluster);
                    ChangeClusterValuesToMean(Cluster);


                }

            }

            return ConvertBackToDoubleArray();
        }


        public List<CategoricalMicroaggregationObject> CreateCluster(CategoricalMicroaggregationObject CenterOfCluster)
        {
            List<CategoricalMicroaggregationObject> PossibleCluster = CalculateDistanceBetweenCenterAndOtherEntries(CenterOfCluster);
            PossibleCluster = PossibleCluster.OrderByDescending(x => x.DistanceToClusterCenter).ToList();
            if (PossibleCluster.Count() < ClusterLimit)
            {
                return PossibleCluster;
            }

            PossibleCluster = PossibleCluster.Take(ClusterLimit).ToList();
            return PossibleCluster;

        }


        public List<CategoricalMicroaggregationObject> CalculateDistanceBetweenCenterAndOtherEntries(CategoricalMicroaggregationObject CenterOfCluster)
        {
            List<CategoricalMicroaggregationObject> PossibleCluster = new List<CategoricalMicroaggregationObject>();

            for (int i = 0; i < Dataset.Length; i++)
            {
                if (Dataset[i].IsPartOfCluster == false)
                {
                    CategoricalMicroaggregationObject TempInstance = new CategoricalMicroaggregationObject();
                    TempInstance.IndexInOrginalDataset = Dataset[i].IndexInOrginalDataset;
                    TempInstance.Row = Dataset[i].Row;
                    TempInstance.DistanceToClusterCenter = calculator.FindCommonElements(CenterOfCluster.Row, Dataset[i].Row);
                    PossibleCluster.Add(TempInstance);
                }
            }

            return PossibleCluster;
        }


        public void ChangeClusterValuesToMean(List<CategoricalMicroaggregationObject> Cluster)
        {
            List<string> MeanRow = CalculateMeanValues(Cluster);
            foreach (CategoricalMicroaggregationObject Row in Cluster)
            {
                Dataset[Row.IndexInOrginalDataset].Row = MeanRow;
                Dataset[Row.IndexInOrginalDataset].IsPartOfCluster = true;
            }
        }


        public List<string> CalculateMeanValues(List<CategoricalMicroaggregationObject> Cluster)
        {
            List<string> MeanRow = new List<string>();

            for (int i = 0; i < Cluster[0].Row.Count(); i++)
            {
                Dictionary<string, int> CommonElementCounter = new Dictionary<string, int>();

                for (int j = 0; j < Cluster.Count(); j++)
                {
                    if(!CommonElementCounter.ContainsKey(Cluster[j].Row[i]))
                    {
                        CommonElementCounter.Add(Cluster[j].Row[i], 1);
                    }

                    else
                    {
                        CommonElementCounter[Cluster[j].Row[i]]++;
                    }
                }

                MeanRow.Add(CommonElementCounter.Aggregate((x, y) => x.Value > y.Value ? x : y).Key);
            }

            return MeanRow;
        }

        public string[,] ConvertBackToDoubleArray()
        {
            string[,] FinalArray = new string[Dataset.Length, Dataset[0].Row.Count()];

            for (int RowIndex = 0; RowIndex < Dataset.GetLength(0); RowIndex++)
            {
                for (int ColumnIndex = 0; ColumnIndex < Dataset[0].Row.Count(); ColumnIndex++)
                {
                    FinalArray[RowIndex, ColumnIndex] = Dataset[RowIndex].Row[ColumnIndex];

                }
            }

            HashSet<string> check = calculator.ConvertColumnToList(FinalArray, FinalArray.GetLength(1) - 1).ToHashSet();
            if (check.Count == 1)
            {
                return null;
            }

            return FinalArray;
        }
    }
}
