﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class RankSwapping
    {
        static Calculator calculator = new Calculator();
        public int RankDiffer;

        public double[,] RankSwapper(double[,] Dataset, int percent)
        {
            RankDiffer = (Dataset.GetLength(0) / 100) * percent;

            for (int ColumnIndex = 0; ColumnIndex < Dataset.GetLength(1); ColumnIndex++)
            {
                List<EntryAndCoordinates> RankList = GetRankList(Dataset, ColumnIndex);

                for (int RowIndex = 0; RowIndex < Dataset.GetLength(0); RowIndex++)
                {
                   
                    int Swap = GetValueToSwap(RankList, RankDiffer, RowIndex);
                    RankList = SwapElements(RankList , RowIndex, Swap);

                }

                Dataset = PutRankListBackIntoDataset(RankList, Dataset);
            }

            return Dataset;
        }


        public static List<EntryAndCoordinates> GetRankList(double[,] Dataset,int ColumnIndex)
        {
            List<EntryAndCoordinates> Interval = new List<EntryAndCoordinates>();
           
                for (int RowIndex = 0; RowIndex < Dataset.GetLength(0); RowIndex++)
                {
                        EntryAndCoordinates EntryObject = new EntryAndCoordinates();
                        EntryObject.Value = Dataset[RowIndex, ColumnIndex];
                        EntryObject.RowIndex = RowIndex;
                        EntryObject.ColumnIndex = ColumnIndex;
                        Interval.Add(EntryObject);
                
                }

            Interval = Interval.OrderBy(x => x.Value).ToList();
            return Interval;
        }


        public static int GetValueToSwap(List<EntryAndCoordinates> Interval, int RankDiffer, int IndexOfValueToSwap )
        {
            Random ran = new Random();
            int IndexOfSecondValueToSwap;


            if (IndexOfValueToSwap + RankDiffer >= Interval.Count() && IndexOfValueToSwap - RankDiffer < 0)
            {
                IndexOfSecondValueToSwap = ran.Next(0, Interval.Count());
            }

            else if (IndexOfValueToSwap - RankDiffer < 0)
            {
                IndexOfSecondValueToSwap = ran.Next(0, IndexOfValueToSwap + RankDiffer);
            }

            else if (IndexOfValueToSwap + RankDiffer >= Interval.Count())
            {
                IndexOfSecondValueToSwap = ran.Next(IndexOfValueToSwap - RankDiffer, Interval.Count());
            }

            else
            {
                IndexOfSecondValueToSwap = ran.Next(IndexOfValueToSwap - RankDiffer, IndexOfValueToSwap + RankDiffer);
            }

            return IndexOfSecondValueToSwap;
        }


        public static List<EntryAndCoordinates> SwapElements(List<EntryAndCoordinates>RankList , int Swap1Index, int Swap2Index)
        {
            int tempCoordinate = RankList[Swap1Index].RowIndex;
            RankList[Swap1Index].RowIndex = RankList[Swap2Index].RowIndex;
            RankList[Swap2Index].RowIndex = tempCoordinate;
            return RankList;
        }


        public static double[,] PutRankListBackIntoDataset(List<EntryAndCoordinates> RankList, double[,] Dataset)
        {
            for(int i = 0; i < RankList.Count(); i++)
            {
                Dataset[RankList[i].RowIndex, RankList[i].ColumnIndex] = RankList[i].Value;
            }

            return Dataset;
        }

    }
}
