﻿using System;
using System.Collections.Generic;
using System.Text;
using OfficeOpenXml;
using MathNet.Numerics.Distributions;

namespace ExcelProtection
{
    public class NoiseAddition
    {
        static Calculator calculator = new Calculator();

        public static double[,] AddNoiseAdditon(double[,] Dataset, double MaxLimit)
        {
            
            List<double> DataList = calculator.ConvertToList(Dataset);    // Convert the dataset to a list
            double StandardDeviation = calculator.CalculateStandardDeviation(DataList);
            MaxLimit = MaxLimit * StandardDeviation / 100;  


            double ProtectedValue;
            MathNet.Numerics.Distributions.Normal normalDist = new Normal(0, MaxLimit);     // For every value in the dataset, add a normally distributed varible generated with a Mean 0 and choosen standard deviation
            double RandomValue;

            for (int i = 0; i < Dataset.GetLength(0); i++)
            {
                for (int j = 0; j < Dataset.GetLength(1); j++)
                {
                    
                    RandomValue = normalDist.Sample();
                    ProtectedValue = (Dataset[i,j] + RandomValue);
                    ProtectedValue = calculator.SetRoundedValue(ProtectedValue);
                    Dataset[i, j] = ProtectedValue;
                }

            }

            return Dataset;    // return the altered dataset


        }

       


    }
}
