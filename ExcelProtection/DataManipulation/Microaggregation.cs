﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class Microaggregation
    {
        public int ClusterLimit;
        public MicroaggregationObject[] Dataset;
        public Calculator calculator = new Calculator();

        public Microaggregation(double[,] _dataset , int _clusterlimit)
        {
            Dataset = new MicroaggregationObject[_dataset.GetLength(0)];
            ClusterLimit = _clusterlimit;

            for (int RowIndex = 0; RowIndex < _dataset.GetLength(0); RowIndex++)
            {
                MicroaggregationObject TransferInstance = new MicroaggregationObject();
                List<double> tempList = new List<double>();

                for (int ColumnIndex = 0; ColumnIndex < _dataset.GetLength(1); ColumnIndex++)
                {

                    tempList.Add(_dataset[RowIndex, ColumnIndex]);
                   
                }

                TransferInstance.Row = tempList;
                TransferInstance.IsPartOfCluster = false;
                TransferInstance.IndexInOrginalDataset = RowIndex;

                Dataset[RowIndex] = TransferInstance;
            }

        }


        public double[,] Microaggregator()
        {
            for (int index = 0; index < Dataset.Length; index++)
            {
                if(Dataset[index].IsPartOfCluster == false)
                {
                    MicroaggregationObject CenterOfCluster = Dataset[index];
                    List<MicroaggregationObject> Cluster = CreateCluster(CenterOfCluster);
                    ChangeClusterValuesToMean(Cluster);
                    

                } 
               
            }
            return ConvertBackToDoubleArray();
        }


        public List<MicroaggregationObject> CreateCluster(MicroaggregationObject CenterOfCluster)
        {
            List<MicroaggregationObject> PossibleCluster = CalculateDistanceBetweenCenterAndOtherEntries(CenterOfCluster);
            PossibleCluster = PossibleCluster.OrderBy(x => x.DistanceToClusterCenter).ToList();
            if(PossibleCluster.Count() < ClusterLimit)
            {
                return PossibleCluster;
            }

            PossibleCluster = PossibleCluster.Take(ClusterLimit).ToList();
            return PossibleCluster;
            
        }


        public List<MicroaggregationObject> CalculateDistanceBetweenCenterAndOtherEntries(MicroaggregationObject CenterOfCluster)
        {
            List<MicroaggregationObject> PossibleCluster = new List<MicroaggregationObject>();

            for(int i = 0; i < Dataset.Length; i++)
            {
                if(Dataset[i].IsPartOfCluster == false)
                {
                    MicroaggregationObject TempInstance = new MicroaggregationObject();
                    TempInstance.IndexInOrginalDataset = Dataset[i].IndexInOrginalDataset;
                    TempInstance.Row = Dataset[i].Row;
                    TempInstance.DistanceToClusterCenter = calculator.CalculateEuclideanDistance(CenterOfCluster.Row, Dataset[i].Row);
                    PossibleCluster.Add(TempInstance);
                }
            }

            return PossibleCluster;
        }


        public void ChangeClusterValuesToMean(List<MicroaggregationObject> Cluster)
        {
            List<double> MeanRow = CalculateMeanValues(Cluster);
            foreach(MicroaggregationObject Row in Cluster)
            {
                Dataset[Row.IndexInOrginalDataset].Row = MeanRow;
                Dataset[Row.IndexInOrginalDataset].IsPartOfCluster = true;
            }
        }


        public List<double> CalculateMeanValues(List<MicroaggregationObject> Cluster)
        {
            List<double> MeanRow = new List<double>();

            for(int i = 0; i < Cluster[0].Row.Count(); i++)
            {
                double sum = 0;

                for(int j = 0; j < Cluster.Count(); j++)
                {
                    sum = sum + Cluster[j].Row[i];
                }
                sum = sum / (double)Cluster.Count();
                sum = calculator.SetRoundedValue(sum);
                MeanRow.Add(sum);
            }

            return MeanRow;
        }

        public double[,] ConvertBackToDoubleArray()
        {
            double[,] FinalArray = new double[Dataset.Length, Dataset[0].Row.Count()];

            for (int RowIndex = 0; RowIndex < Dataset.GetLength(0); RowIndex++)
            {
                for (int ColumnIndex = 0; ColumnIndex < Dataset[0].Row.Count(); ColumnIndex++)
                {
                    FinalArray[RowIndex, ColumnIndex] = Dataset[RowIndex].Row[ColumnIndex];

                }
            }

            return FinalArray;
        }



    }
}
