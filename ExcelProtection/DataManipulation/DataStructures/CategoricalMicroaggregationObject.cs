﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class CategoricalMicroaggregationObject
    {
        public List<string> Row { get; set; }

        public int IndexInOrginalDataset { get; set; }

        public bool IsPartOfCluster { get; set; }

        public double DistanceToClusterCenter { get; set; }
    }
}
