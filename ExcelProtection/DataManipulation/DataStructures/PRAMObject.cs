﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class PRAMObject
    {
        public double LowerBound;

        public double HigherBound;

        public string Value;

        public PRAMObject(double x, double y, string data)
        {
            LowerBound = x;
            HigherBound = y;
            Value = data;
        }

        public bool CheckIfInRange(double x)
        {
            if (x > LowerBound && x <= HigherBound)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
