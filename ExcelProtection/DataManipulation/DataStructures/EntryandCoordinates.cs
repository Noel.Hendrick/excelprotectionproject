﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class EntryAndCoordinates
    {
        public double Value { get; set; }

        public int RowIndex { get; set; }

        public int ColumnIndex { get; set; }
    }
}
