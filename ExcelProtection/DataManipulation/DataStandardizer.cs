﻿using System;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;

namespace ExcelProtection
{
    public class DataStandardizer
    {
        static Calculator calculator = new Calculator();
        
        public double[,] DataStandardizing(double[,] Dataset)
        {
            
            var DatasetAnalytics = CalculateAnalytics(Dataset);       // Call the calculate analytics method for the Dataset
            double Mean = DatasetAnalytics.Item1;
            double StandardDeviation = DatasetAnalytics.Item2;
            double StandardisedValue = 0;
            

            for (int i = 0; i < Dataset.GetLength(0); i++)      // For every value in the Dataset, subtract the mean and divide by standard deviation
            {
                for (int j = 0; j < Dataset.GetLength(1); j++)
                {
                   
                    StandardisedValue = (((Dataset[i,j]) - Mean) / StandardDeviation);
                    StandardisedValue = calculator.SetRoundedValue(StandardisedValue);
                    Dataset[i, j] = StandardisedValue;
                }

            }

            return Dataset;
        }

        public static Tuple<Double, Double> CalculateAnalytics(double[,] Dataset)
        {

            // convert to a list and then calculate the standard deviation and mean method, return a tuple of these two values
            List<double> dataset = calculator.ConvertToList(Dataset);  
            return calculator.ReturnStandardDeviationAndMean(dataset);

        }

    }
}
