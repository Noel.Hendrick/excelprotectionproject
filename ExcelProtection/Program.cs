﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using OfficeOpenXml;
using ZedGraph;

namespace ExcelProtection
{
    class Program
    {
        public static string UseStatistic;

        public static void Main()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            Console.WriteLine("Enter 1 for Numeric or anything else for Categorical");
            string decider = Console.ReadLine();
            Console.WriteLine("Enter 1 for Statistical Information Loss or anything else for Machine Learning");
            UseStatistic = Console.ReadLine();


            if (decider == "1")
            {

                //Retrives the Orginal Excelsheet
                double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CASCrefmicrodata.xlsx", "Census", 2, 1);

                //Calls the data standizer method and saves the result in a new file
                Dataset = GetNormalisedData(Dataset);
                excelHandler.SaveAsNewFile(Dataset, @"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx");

                // Calls the Protection method and saves the result in a new file
                Dataset = NumericProtectData(Dataset);
                excelHandler.SaveAsNewFile(Dataset, @"C:\Users\USER\Downloads\TestExcelsheets\DataWithNoise.xlsx");

            }

            else
            {
                //Retrives the Orginal Excelsheet
                string[,] Dataset = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);

                //Calls The Categorical Protection Method 
                Dataset = CategoricalProtectData(Dataset);
                excelHandler.SaveAsNewFile(Dataset, @"C:\Users\USER\Downloads\TestExcelsheets\CategoricalProtectedData.xlsx");

            }






        }

        public static double[,] GetNormalisedData(double[,] Dataset)
        {
            //Creates an instance of the DataStandardizer and call the Data Standardizing method and then return the changed Data
            DataStandardizer DataChanger = new DataStandardizer();
            Dataset = DataChanger.DataStandardizing(Dataset);
            return Dataset;


        }

        public static double[,] NumericProtectData(double[,] Dataset)
        {
            Console.WriteLine("Enter 1 for Genetic Disclosure Risk and anything else for Standard Distance");
            string UseGenetic = Console.ReadLine();

            if(UseGenetic != "1" && UseStatistic == "1")
            {
                Dataset = NumericGenetic.StartSimulation(Dataset, true, false);
                return Dataset;
            }

            else if(UseGenetic == "1" && UseStatistic == "1")
            {
                Dataset = NumericGenetic.StartSimulation(Dataset, true, true);
                return Dataset;
            }

            else if(UseGenetic != "1" && UseStatistic != "1")
            {
                Dataset = NumericGenetic.StartSimulation(Dataset, false, false);
                return Dataset;
            }

            else
            {
                Console.WriteLine("WARNING: The runtime of this particular configuration is very large");
                Dataset = NumericGenetic.StartSimulation(Dataset, false, true);
                return Dataset;
            }

        }

        public static string[,] CategoricalProtectData(string[,] Dataset)
        {
            if (UseStatistic == "1")
            {

                Dataset = CategoricalGenetic.StartSimulation(Dataset, true);
                return Dataset;
            }

            else
            {

                Dataset = CategoricalGenetic.StartSimulation(Dataset, false);
                return Dataset;
            }
        }

    }
}
