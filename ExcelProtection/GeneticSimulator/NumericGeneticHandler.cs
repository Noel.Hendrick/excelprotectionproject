﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class NumericGeneticHandler
    {
        public static Random ran = new Random();
        public static Calculator calculator = new Calculator();

        public static List<NumericGeneticObject> NumericCreateIntialPopulation(double[,] Dataset)
        {
            List<NumericGeneticObject> Population = new List<NumericGeneticObject>();
            for(int i = 0; i<30; i++)
            {
                Population.Add(CreateRandomNumericObject(Dataset));
            }

            return Population;
        }

        public static NumericGeneticObject CreateRandomNumericObject(double[,] Dataset)
        {

            List<string> possibleorder = new List<string>() { "RankSwapping", "MicroAggregation", "NoiseAddition" };
            NumericGeneticObject temp = new NumericGeneticObject();

            temp.RankSwappingParameter = ran.Next(1, 10) * 10;
            temp.NoiseAdditionParameter =  ran.Next(1, 95);
            temp.MicroaggregationParameter = ran.Next(1,(Dataset.GetLength(0)/5)) * 5;
            List<string> Order = new List<string>();
            
            for(int i = 0; i < 3; i++)
            {
                int select = ran.Next(0, possibleorder.Count);
                Order.Add(possibleorder[select]);
                possibleorder.RemoveAt(select);
                
            }
            temp.Order = Order;

            return temp;
            
        }

        public static List<NumericGeneticObject> CalculateNumericPopulationFitness(List<NumericGeneticObject> Population, double[,] Dataset, bool UseGenticDR)
        {
            NumericProtectionCalculater calc = new NumericProtectionCalculater();
            foreach(NumericGeneticObject configuration in Population)
            {
                if (configuration.Fitness == 0)
                {
                    double MainAverage = 0;
                    double DisclosureRisk = 0;
                    double InformationLoss = 0;

                    for (int i = 0; i < 3; i++)
                    {
                        double[,] _dataset = Dataset;
                        _dataset = MaskData(configuration, _dataset);
                        var result = calc.CalculateQualityOfData(_dataset, UseGenticDR);
                        MainAverage = MainAverage + result.Item1;
                        DisclosureRisk = DisclosureRisk + result.Item2;
                        InformationLoss = InformationLoss + result.Item3;
                    }

                    MainAverage = MainAverage / 3;
                    DisclosureRisk = DisclosureRisk / 3;
                    InformationLoss = InformationLoss / 3;

                    configuration.Fitness = MainAverage;
                    configuration.InformationLoss = InformationLoss;
                    configuration.DisclosureRisk = DisclosureRisk;
                }
            }

            return Population;
        }

        public static List<NumericGeneticObject> CalculateNumericMLPopulationFitness(List<NumericGeneticObject> Population, double[,] Dataset, bool UseGeneticDR)
        {
            ExcelHandler excelHandler = new ExcelHandler();
            NumericMLProtectionCalculater calc = new NumericMLProtectionCalculater(Dataset);
            double OrginalAccuracy = calc.CalculateAccuracy(Dataset);

            foreach (NumericGeneticObject configuration in Population)
            {
                if (configuration.Fitness == 0)
                {
                    double Fitness = 0;
                    double DisclosureRisk = 0;
                    double InformationLoss = 0;

                    for (int i = 0; i < 3; i++)
                    {
                        double[,] _dataset = Dataset;
                        _dataset = MaskData(configuration, _dataset);
                         InformationLoss = InformationLoss + (calc.CalculateAccuracy(_dataset)- OrginalAccuracy);

                        if (InformationLoss < 0)
                        {
                            InformationLoss = 0;
                        }

                        if (UseGeneticDR == false)
                        {
                            DisclosureRisk = NumericProtectionCalculater.CalculateDisclosureRisk(_dataset, Dataset);
                        }

                        else
                        {
                            DisclosureRisk = DistanceGenetic.StartSimulation(_dataset, Dataset);
                        }

                    }

                    DisclosureRisk = DisclosureRisk / 3;
                    InformationLoss = InformationLoss / 3;
                    if (InformationLoss > 100)
                    {
                        InformationLoss = 100;
                    }
                    Fitness = (DisclosureRisk + InformationLoss)/2;

                    DisclosureRisk = calculator.SetRoundedValue(DisclosureRisk);
                    InformationLoss = calculator.SetRoundedValue(InformationLoss);
                    Fitness = calculator.SetRoundedValue(Fitness);

                    configuration.Fitness = Fitness;
                    configuration.InformationLoss = InformationLoss;
                    configuration.DisclosureRisk = DisclosureRisk;
                }
            }

            return Population;
        }


        public static List<NumericGeneticObject> CreateNewPopulationFromExistingPopulation(List<NumericGeneticObject> Population, double[,] Dataset)
        {
            List<NumericGeneticObject> NewPopulation = AsexualRecombination(Population);

            //TODO try swap traits from other well preforming configs instead of just randomly changing
            for(int i = 0; i<10;i++)
            {
                NewPopulation.Add(Mutate(NewPopulation[i], Dataset));
            }

            List<NumericGeneticObject> MatingPool = GenerateMatingPool(Population);
            for (int  i = 0; i < 10; i++)
            {   
                NumericGeneticObject Chromosome = Recombination(MatingPool);
                if (i % 3 == 0)
                {
                    Chromosome = Mutate(Chromosome, Dataset);
                }
                NewPopulation.Add(Chromosome);
            }

            return NewPopulation;
        }

        public static List<NumericGeneticObject> AsexualRecombination(List<NumericGeneticObject> Population)
        {
            Population = Population.OrderBy(x => x.Fitness).ToList();
            List<NumericGeneticObject> NewPopulation = new List<NumericGeneticObject>();
            for (int i = 0; i < 10; i++)
            {
                NewPopulation.Add(Population[i]);
            }

            return NewPopulation;
        }

        public static List<NumericGeneticObject> GenerateMatingPool(List<NumericGeneticObject> Population)
        {
            List<NumericGeneticObject> MatingPool = new List<NumericGeneticObject>();

            for (int i = 0; i < Population.Count(); i++)
            {
                if (Population[i].Fitness <= 1 && Population[i].Fitness >= 0)
                {
                    for (int j = 0; j < (1000 - Population[i].Fitness * 1000); j++)
                    {
                        MatingPool.Add(Population[i]);
                    }
                }

                else if (Population[i].Fitness > 1 && Population[i].Fitness <= 5)
                {
                    for (int j = 0; j < 500; j++)
                    {
                        MatingPool.Add(Population[i]);
                    }
                }

                else if (Population[i].Fitness > 5 && Population[i].Fitness <= 10)
                {
                    for (int j = 0; j < 300; j++)
                    {
                        MatingPool.Add(Population[i]);
                    }
                }

                else if (Population[i].Fitness > 10 && Population[i].Fitness <= 30)
                {
                    for (int j = 0; j < 100; j++)
                    {
                        MatingPool.Add(Population[i]);
                    }
                }

                else
                {
                    for (int j = 0; j < 50; j++)
                    {
                        MatingPool.Add(Population[i]);
                    }
                }
            }

            return MatingPool;
        }

        public static NumericGeneticObject Recombination(List<NumericGeneticObject> MatingPool)
        {

            NumericGeneticObject Parent1 = MatingPool[ran.Next(0, MatingPool.Count())];
            NumericGeneticObject Parent2 = MatingPool[ran.Next(0, MatingPool.Count())];
            NumericGeneticObject Child = new NumericGeneticObject();

            int decision = ran.Next(0, 2);
            if(decision == 0)
            {
                Child.MicroaggregationParameter = Parent1.MicroaggregationParameter;
            }

            else
            {
                Child.MicroaggregationParameter = Parent2.MicroaggregationParameter;
            }

            decision = ran.Next(0, 2);
            if (decision == 0)
            {
                Child.RankSwappingParameter = Parent1.RankSwappingParameter;
            }

            else
            {
                Child.RankSwappingParameter = Parent2.RankSwappingParameter;
            }

            decision = ran.Next(0, 2);
            if (decision == 0)
            {
                Child.NoiseAdditionParameter = Parent1.NoiseAdditionParameter;
            }

            else
            {
                Child.NoiseAdditionParameter = Parent2.NoiseAdditionParameter;
            }


            decision = ran.Next(0, 2);
            if (decision == 0)
            {
                Child.Order = Parent1.Order;
            }

            else
            {
                Child.Order = Parent2.Order;
            }

            return Child;

        }

        public static NumericGeneticObject Mutate(NumericGeneticObject Chromosome, double[,] Dataset)
        {
            int AttributeToChange = ran.Next(0, 4);
            if(AttributeToChange == 0)
            {
                Chromosome.MicroaggregationParameter = ran.Next(1, (Dataset.GetLength(0) / 5)) * 5;
            }

            else if(AttributeToChange == 1)
            {
                Chromosome.RankSwappingParameter = ran.Next(1, 10) * 10;
            }

            else if (AttributeToChange == 2)
            {
                Chromosome.NoiseAdditionParameter = ran.Next(1, 95);
            }

            else
            {
                List<string> possibleorder = new List<string>() { "RankSwapping", "MicroAggregation", "NoiseAddition" };
                List<string> Order = new List<string>();

                for (int i = 0; i < 3; i++)
                {
                    int select = ran.Next(0, possibleorder.Count);
                    Order.Add(possibleorder[select]);
                    possibleorder.RemoveAt(select);

                }

                Chromosome.Order = Order;

            }

            return Chromosome;
        }

        public static double[,] MaskData(NumericGeneticObject configuration, double [,] Dataset)
        {
            for (int i = 0; i < 3; i++)
            {
                if (configuration.Order[i] == "MicroAggregation")
                {
                    Microaggregation micro = new Microaggregation(Dataset, configuration.MicroaggregationParameter);
                    Dataset = micro.Microaggregator();
                }

                if (configuration.Order[i] == "NoiseAddition")
                {
                    Dataset = NoiseAddition.AddNoiseAdditon(Dataset, configuration.NoiseAdditionParameter);
                }

                if (configuration.Order[i] == "RankSwapping")
                {
                    RankSwapping rank = new RankSwapping();
                    Dataset = rank.RankSwapper(Dataset, configuration.RankSwappingParameter);
                }

            }

            return Dataset;
        }

        public static double GeneticDisclosureRisk(double[,] ProtectedDataset, double[,] OriginalDataset)
        {
            return DistanceGenetic.StartSimulation(OriginalDataset, ProtectedDataset);
        }
    }
}
