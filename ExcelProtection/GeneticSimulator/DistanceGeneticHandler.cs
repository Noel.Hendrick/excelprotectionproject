﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class DistanceGeneticHandler
    {
        public static List<DistanceGeneticObject> CreateIntialPopulation(double[,] OriginalDataset)
        {
            Random ran = new Random();

            List<DistanceGeneticObject> population = new List<DistanceGeneticObject>();
            population.Add(AddStandardDistance(OriginalDataset));


            for(int i = 0; i<19 ;i++)
            {
                DistanceGeneticObject temp = new DistanceGeneticObject();
                List<double> Weights = new List<double>();

                for(int j = 0; j<OriginalDataset.GetLength(1); j++)
                {
                    Weights.Add(ran.NextDouble());
                }
                temp.Weights = Weights;
                population.Add(temp);
            }

            return population;
        }

        public static List<DistanceGeneticObject> CalculatePopulationFitness(double[,] OriginalDataset, double[,] ProtectedDataset, List<DistanceGeneticObject> Population)
        {
            for(int i = 0; i<Population.Count; i++)
            {
                Population[i] = NumericProtectionCalculater.CalculateWeightedDisclosureRisk(OriginalDataset, ProtectedDataset, Population[i]);
            }

            return Population;
        }

        public static DistanceGeneticObject AddStandardDistance(double[,] OriginalDataset)
        {
            DistanceGeneticObject temp = new DistanceGeneticObject();
            List<double> weights = new List<double>();

            for(int  i = 0; i<OriginalDataset.GetLength(1); i++)
            {
                weights.Add(1);
            }

            temp.Weights = weights;
            return temp;
        }

        public static List<DistanceGeneticObject> CreateNewPopulationFromExistingPopulation(List<DistanceGeneticObject> Population)
        {
            List<DistanceGeneticObject> NewPopulation = AsexualRecombination(Population);

            //TODO try swap traits from other well preforming configs instead of just randomly changing
            for (int i = 0; i < 5; i++)
            {
                NewPopulation.Add(Mutate(NewPopulation[i]));
            }

            List<DistanceGeneticObject> MatingPool = GenerateMatingPool(Population);
            for (int i = 0; i < 10; i++)
            {
                DistanceGeneticObject Chromosome = Recombination(MatingPool);
                if (i % 3 == 0)
                {
                    Chromosome = Mutate(Chromosome);
                }
                NewPopulation.Add(Chromosome);
            }

            return NewPopulation;
        }

        public static List<DistanceGeneticObject> AsexualRecombination(List<DistanceGeneticObject> Population)
        {
            Population = Population.OrderByDescending(x => x.Fitness).ToList();
            List<DistanceGeneticObject> NewPopulation = new List<DistanceGeneticObject>();
            for (int i = 0; i < 5; i++)
            {
                NewPopulation.Add(Population[i]);
            }

            return NewPopulation;
        }

        public static DistanceGeneticObject Mutate(DistanceGeneticObject Chromosome)
        {
            Random ran = new Random();
            int AttributeToChange = ran.Next(0, Chromosome.Weights.Count);
            Chromosome.Weights[AttributeToChange] = ran.NextDouble() * 2;
            return Chromosome;
        }

        public static List<DistanceGeneticObject> GenerateMatingPool(List<DistanceGeneticObject> Population)
        {
            List<DistanceGeneticObject> MatingPool = new List<DistanceGeneticObject>();

            for (int i = 0; i < Population.Count(); i++)
            {
                if (Population[i].Fitness == 0)
                {
                    MatingPool.Add(Population[i]);
                }

                else
                {
                    for (int j = 0; j < (10 * Population[i].Fitness); j++)
                    {
                        MatingPool.Add(Population[i]);
                    }
                }
            }

            return MatingPool;
        }

        public static DistanceGeneticObject Recombination(List<DistanceGeneticObject> MatingPool)
        {

            Random ran = new Random();
            DistanceGeneticObject Parent1 = MatingPool[ran.Next(0, MatingPool.Count())];
            DistanceGeneticObject Parent2 = MatingPool[ran.Next(0, MatingPool.Count())];
            DistanceGeneticObject Child = new DistanceGeneticObject();
            List<double> temp = new List<double>();

            for (int i = 0; i < Parent1.Weights.Count; i++)
            {
                int decision = ran.Next(0, 2);
                if (decision == 0)
                {
                    temp.Add(Parent1.Weights[i]);
                }

                else
                {
                    temp.Add(Parent2.Weights[i]);
                }
            }

            Child.Weights = temp;
            return Child;

        }


    }
}
