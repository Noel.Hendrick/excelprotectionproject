﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace ExcelProtection
{
    class CategoricalGenetic
    {

        public static UIObject Transfer = new UIObject();
        public static Thread thread;
        public static bool UseStatistic;

        private static object lockObj = new object();

        private static UIHandler _graph;
        public static UIHandler Graph
        {
            get
            {
                lock(lockObj)
                {
                    return _graph;
                }
            }
            set
            {
                lock(lockObj)
                {
                    _graph = value;
                }
            }
        }

        public static string[,] StartSimulation(string[,] Dataset, bool _useStatistic)
        {
            UseStatistic = _useStatistic;
            CreateUI();
            List<CategoricalGeneticObject> Population = CategoricalGeneticHandler.CategoricalCreateIntialPopulation(Dataset);
            if (UseStatistic == true)
            {
                Population = CategoricalGeneticHandler.CalculateCategoricalPopulationFitness(Population, Dataset);
            }

            else
            {
                Population = CategoricalGeneticHandler.CalculateCategoricalMLPopulationFitness(Population, Dataset);
            }
            int genNum = 1;
            UpdateTransfer(Population, genNum);

            // Run for 50 Generations
            for (int i = 0; i < 50; i++)
            {
                Population = CategoricalGeneticHandler.CreateNewPopulationFromExistingPopulation(Population, Dataset);
                if (UseStatistic == true)
                {
                    Population = CategoricalGeneticHandler.CalculateCategoricalPopulationFitness(Population, Dataset);
                }

                else
                {
                    Population = CategoricalGeneticHandler.CalculateCategoricalMLPopulationFitness(Population, Dataset);
                }
                genNum++;
                UpdateTransfer(Population,genNum);

            }

            Population.OrderBy(x => x.Fitness);
            Dataset = CategoricalGeneticHandler.MaskData(Population[0], Dataset);

            return Dataset;

        }


        public static double GetAverageFitness(List<CategoricalGeneticObject> Population)
        {
            double sum = 0;
            foreach (CategoricalGeneticObject configuration in Population)
            {
                sum = sum + configuration.Fitness;
            }

            return sum / Population.Count();

        }


        public static void RunUI()
        {
            Graph = new UIHandler(Transfer);
            Graph.Text = "Simulation";
            Application.Run(Graph);
        }

        public static void UpdateTransfer(List<CategoricalGeneticObject> Population, int GenNumber)
        {
            PointPairList list = new PointPairList();
            foreach(CategoricalGeneticObject config in Population)
            {
                list.Add(config.InformationLoss, config.DisclosureRisk);
            }

            Transfer.UseStatistic = UseStatistic;
            Transfer.PointsForPopulation = list;
            Transfer.AverageFitness = GetAverageFitness(Population);
            Transfer.MaxFitness = Population.Min(x => x.Fitness);
            Transfer.GenerationNumber = GenNumber;

            Graph.UpdateUI(Transfer);

        }


        public static void CreateUI()
        {
            thread = new Thread(() => RunUI());
            thread.Start();
        }
    }
}
