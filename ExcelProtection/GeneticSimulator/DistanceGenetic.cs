﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class DistanceGenetic
    {

        public static double StartSimulation(double[,] OriginalDataset, double[,] ProtectedDataset)
        {
            List<DistanceGeneticObject> Population = DistanceGeneticHandler.CreateIntialPopulation(OriginalDataset);
            Population = DistanceGeneticHandler.CalculatePopulationFitness(OriginalDataset, ProtectedDataset, Population);

            // Run for 20 Generations
            for (int i = 0; i < 20; i++)
            {
                Population = DistanceGeneticHandler.CreateNewPopulationFromExistingPopulation(Population);
                Population = DistanceGeneticHandler.CalculatePopulationFitness(OriginalDataset, ProtectedDataset, Population);
            }

            Population.OrderByDescending(x => x.Fitness);
            double disclosureRisk = (((double)Population[0].Fitness / ProtectedDataset.GetLength(0)) * 100);

            Calculator calc = new Calculator();
            disclosureRisk = calc.SetRoundedValue(disclosureRisk);

            return disclosureRisk;

        }

    }
}
