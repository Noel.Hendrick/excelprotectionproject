﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    class CategoricalGeneticObject
    {
        public double PRAMParameter { get; set; }
        public int MicroaggregationParameter { get; set; }
        public List<string> Order { get; set; }
        public double Fitness { get; set; }
        public double InformationLoss { get; set; }
        public double DisclosureRisk { get; set; }
    }
}
