﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class NumericGeneticObject
    {
        public int NoiseAdditionParameter { get; set; }
        public int RankSwappingParameter { get; set; }
        public int MicroaggregationParameter { get; set; }
        public List<string> Order { get; set; }
        public double Fitness { get; set; }
        public double InformationLoss { get; set; }
        public double DisclosureRisk { get; set; }

       
    }
}
