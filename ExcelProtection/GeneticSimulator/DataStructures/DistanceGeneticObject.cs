﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class DistanceGeneticObject
    {
        public List<double> Weights { get; set; }
        public int Fitness { get; set; }

    }
}
