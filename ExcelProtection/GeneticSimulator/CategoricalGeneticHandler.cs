﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    class CategoricalGeneticHandler
    {
        public static Random ran = new Random();

        public static Calculator calculator = new Calculator();
        public static List<CategoricalGeneticObject> CategoricalCreateIntialPopulation(string[,] Dataset)
        {
            List<CategoricalGeneticObject> Population = new List<CategoricalGeneticObject>();
            for (int i = 0; i < 30; i++)
            {
                Population.Add(CreateRandomCategoricalObject(Dataset));
            }

            return Population;
        }

        public static CategoricalGeneticObject CreateRandomCategoricalObject(string[,] Dataset)
        {

            List<string> possibleorder = new List<string>() { "PRAM", "MicroAggregation"};
            CategoricalGeneticObject temp = new CategoricalGeneticObject();

            temp.PRAMParameter = ran.NextDouble();
            temp.MicroaggregationParameter = ran.Next(1, (Dataset.GetLength(0) / 5)) * 5;
            List<string> Order = new List<string>();

            for (int i = 0; i < 2; i++)
            {
                int select = ran.Next(0, possibleorder.Count);
                Order.Add(possibleorder[select]);
                possibleorder.RemoveAt(select);

            }
            temp.Order = Order;

            return temp;

        }

        public static List<CategoricalGeneticObject> CalculateCategoricalPopulationFitness(List<CategoricalGeneticObject>Population, string[,] Dataset)
        {
            CategoricalProtectionCalculater calc = new CategoricalProtectionCalculater();
            foreach (CategoricalGeneticObject configuration in Population)
            {
                if (configuration.Fitness == 0)
                {
                    double MainAverage = 0;
                    double DisclosureRisk = 0;
                    double InformationLoss = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        string[,] _dataset = Dataset; 
                        _dataset = MaskData(configuration, _dataset);
                        var result = calc.CalculateQualityOfData(_dataset);
                        MainAverage = MainAverage + result.Item1;
                        DisclosureRisk = DisclosureRisk + result.Item2;
                        InformationLoss = InformationLoss = result.Item3;
                        
                    }
                    MainAverage = MainAverage / 3;
                    DisclosureRisk = DisclosureRisk / 3;
                    InformationLoss = InformationLoss / 3;



                    configuration.Fitness = MainAverage;
                    configuration.InformationLoss = InformationLoss;
                    configuration.DisclosureRisk = DisclosureRisk;
                }
            }

            return Population;
        }

        public static List<CategoricalGeneticObject> CalculateCategoricalMLPopulationFitness(List<CategoricalGeneticObject> Population, string[,] Dataset)
        {
            ExcelHandler excelHandler = new ExcelHandler();
            CategoricalMLProtectionCalculater calc = new CategoricalMLProtectionCalculater(Dataset);
            double OrginalAccuracy = calc.CalculateAccuracy(Dataset);

            foreach (CategoricalGeneticObject configuration in Population)
            {
                if (configuration.Fitness == 0)
                {
                    double Fitness = 0;
                    double DisclosureRisk = 0;
                    double InformationLoss = 0;

                    for (int i = 0; i < 3; i++)
                    {
                        string[,] _dataset = Dataset;
                        _dataset = MaskData(configuration, _dataset);
                        InformationLoss = InformationLoss + ( OrginalAccuracy - calc.CalculateAccuracy(_dataset));

                        if(InformationLoss < 0)
                        {
                            InformationLoss = 0;
                        }

                        DisclosureRisk = CategoricalProtectionCalculater.CalculateDisclosureRisk(_dataset, Dataset);

                    }

                    
                    DisclosureRisk = DisclosureRisk / 3;
                    InformationLoss = InformationLoss / 3;
                    Fitness = (DisclosureRisk + InformationLoss) / 2;

                    DisclosureRisk = calculator.SetRoundedValue(DisclosureRisk);
                    InformationLoss = calculator.SetRoundedValue(InformationLoss);
                    Fitness = calculator.SetRoundedValue(Fitness);

                    configuration.Fitness = Fitness;
                    configuration.InformationLoss = InformationLoss;
                    configuration.DisclosureRisk = DisclosureRisk;
                }
            }

            return Population;
        }


        public static List<CategoricalGeneticObject> CreateNewPopulationFromExistingPopulation(List<CategoricalGeneticObject> Population, string[,] Dataset)
        {
            List<CategoricalGeneticObject> NewPopulation = AsexualRecombination(Population);
            for (int i = 0; i < 10; i++)
            {
                NewPopulation.Add(Mutate(NewPopulation[i], Dataset));
            }

            List<CategoricalGeneticObject> MatingPool = GenerateMatingPool(Population);
            for (int i = 0; i < 10; i++)
            {
                CategoricalGeneticObject Chromosome = Recombination(MatingPool);
                if (i % 3 == 0)
                {
                    Chromosome = Mutate(Chromosome, Dataset);
                }
                NewPopulation.Add(Chromosome);
            }

            return NewPopulation;
        }

        public static List<CategoricalGeneticObject> AsexualRecombination(List<CategoricalGeneticObject> Population)
        {
            Population = Population.OrderBy(x => x.Fitness).ToList();
            List<CategoricalGeneticObject> NewPopulation = new List<CategoricalGeneticObject>();
            for (int i = 0; i < 10; i++)
            {
                NewPopulation.Add(Population[i]);
            }

            return NewPopulation;
        }

        public static List<CategoricalGeneticObject> GenerateMatingPool(List<CategoricalGeneticObject> Population)
        {
            List<CategoricalGeneticObject> MatingPool = new List<CategoricalGeneticObject>();

            for (int i = 0; i < Population.Count(); i++)
            {
                for (int j = 0; j < (1000 - Population[i].Fitness * 1000); j++)
                {
                    MatingPool.Add(Population[i]);
                }
            }

            return MatingPool;
        }

        public static CategoricalGeneticObject Recombination(List<CategoricalGeneticObject> MatingPool)
        {

            CategoricalGeneticObject Parent1 = MatingPool[ran.Next(0, MatingPool.Count())];
            CategoricalGeneticObject Parent2 = MatingPool[ran.Next(0, MatingPool.Count())];
            CategoricalGeneticObject Child = new CategoricalGeneticObject();

            int decision = ran.Next(0, 2);
            if (decision == 0)
            {
                Child.MicroaggregationParameter = Parent1.MicroaggregationParameter;
            }

            else
            {
                Child.MicroaggregationParameter = Parent2.MicroaggregationParameter;
            }

            decision = ran.Next(0, 2);
            if (decision == 0)
            {
                Child.PRAMParameter = Parent1.PRAMParameter;
            }

            else
            {
                Child.PRAMParameter = Parent2.PRAMParameter;
            }

            decision = ran.Next(0, 2);
            if (decision == 0)
            {
                Child.Order = Parent1.Order;
            }

            else
            {
                Child.Order = Parent2.Order;
            }

            return Child;

        }

        public static CategoricalGeneticObject Mutate(CategoricalGeneticObject Chromosome, string[,] Dataset)
        {
            int AttributeToChange = ran.Next(0, 3);
            if (AttributeToChange == 0)
            {
                Chromosome.MicroaggregationParameter = ran.Next(1, (Dataset.GetLength(0) / 5)) * 5;
            }

            else if (AttributeToChange == 1)
            {
                Chromosome.PRAMParameter = ran.NextDouble();
            }

            else
            {
                List<string> possibleorder = new List<string>() { "PRAM", "MicroAggregation" };
                List<string> Order = new List<string>();

                for (int i = 0; i < 2; i++)
                {
                    int select = ran.Next(0, possibleorder.Count);
                    Order.Add(possibleorder[select]);
                    possibleorder.RemoveAt(select);

                }

                Chromosome.Order = Order;

            }

            return Chromosome;
        }

        public static string[,] MaskData(CategoricalGeneticObject configuration, string[,] Dataset)
        {
            for (int i = 0; i < 2; i++)
            {
                if (configuration.Order[i] == "MicroAggregation")
                {
                    string[,] temp;
                    CategoricalMicroaggregation micro = new CategoricalMicroaggregation(Dataset, configuration.MicroaggregationParameter);
                    temp = micro.Microaggregator();
                    if(temp != null)
                    {
                        Dataset = temp;
                    }

                }

                if (configuration.Order[i] == "PRAM")
                {
                    PRAM pram = new PRAM();
                    Dataset = pram.ReturnPRAM(Dataset, configuration.PRAMParameter);
                }

            }

            return Dataset;
        }
    }
}
