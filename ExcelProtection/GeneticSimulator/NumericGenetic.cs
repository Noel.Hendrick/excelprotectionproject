﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MoreLinq;
using ZedGraph;

namespace ExcelProtection
{
    public class NumericGenetic
    {
        public static UIObject Transfer = new UIObject();
        public static Thread thread;
        public static bool UseStatistic;
        public static bool UseGeneticDR;

        private static object lockObj = new object();

        private static UIHandler _graph;
        public static UIHandler Graph
        {
            get
            {
                lock (lockObj)
                {
                    return _graph;
                }
            }
            set
            {
                lock (lockObj)
                {
                    _graph = value;
                }
            }
        }

        public static double[,] StartSimulation(double[,] Dataset, bool _useStatistic, bool _useGeneticDR)
        {
            UseStatistic = _useStatistic;
            UseGeneticDR = _useGeneticDR;

            CreateUI();
            List<NumericGeneticObject>Population = NumericGeneticHandler.NumericCreateIntialPopulation(Dataset);
            if (UseStatistic == true)
            {
                Population = NumericGeneticHandler.CalculateNumericPopulationFitness(Population, Dataset, UseGeneticDR);
            }

            else
            {
                Population = NumericGeneticHandler.CalculateNumericMLPopulationFitness(Population, Dataset, UseGeneticDR);
            }
            int genNum = 1;
            UpdateTransfer(Population, genNum);

            // Run for 50 Generations
            for (int i = 0; i<50;i++)
            {
                Population = NumericGeneticHandler.CreateNewPopulationFromExistingPopulation(Population, Dataset);

                if (UseStatistic == true)
                {
                    Population = NumericGeneticHandler.CalculateNumericPopulationFitness(Population, Dataset, UseGeneticDR);
                }

                else
                {
                    Population = NumericGeneticHandler.CalculateNumericMLPopulationFitness(Population, Dataset, UseGeneticDR);
                }
                genNum++;
                UpdateTransfer(Population, genNum);



            }

            Population.OrderBy(x => x.Fitness);
            Dataset = NumericGeneticHandler.MaskData(Population[0], Dataset);

            return Dataset;
            
        }


        public static double GetAverageFitness(List<NumericGeneticObject> Population)
        {
            double sum = 0;
            foreach(NumericGeneticObject configuration in Population)
            {
                sum = sum + configuration.Fitness;
            }

            return sum / Population.Count();

        }

        public static void RunUI()
        {
            Graph = new UIHandler(Transfer);
            Graph.Text = "Simulation";
            Application.Run(Graph);
        }

        public static void UpdateTransfer(List<NumericGeneticObject> Population, int GenNumber)
        {
            PointPairList list = new PointPairList();
            foreach (NumericGeneticObject config in Population)
            {
                list.Add(config.InformationLoss, config.DisclosureRisk);
            }

            Transfer.UseStatistic = UseStatistic;
            Transfer.PointsForPopulation = list;
            Transfer.AverageFitness = GetAverageFitness(Population);
            Transfer.MaxFitness = Population.Min(x => x.Fitness);
            Transfer.GenerationNumber = GenNumber;
            Graph.UpdateUI(Transfer);

        }


        public static void CreateUI()
        {
            thread = new Thread(() => RunUI());
            thread.Start();
        }



    }
}
