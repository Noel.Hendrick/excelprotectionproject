﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML;

namespace ExcelProtection
{
    public class NumericMLProtectionCalculater
    {
        public static MLContext mlContext = new MLContext();
        public static List<NumericModelIn> TestSet = new List<NumericModelIn>();
        public static Calculator calc = new Calculator();

        public NumericMLProtectionCalculater(double[,] _dataset)
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] OrginalDataset = _dataset;
            int TrainingSetRowLimit = (int)(OrginalDataset.GetLength(0) * 0.75);
            for (int i = TrainingSetRowLimit; i < OrginalDataset.GetLength(0); i++)
            {
                TestSet.Add(new NumericModelIn
                {
                    V0 = (float)OrginalDataset[i, 0],
                    V1 = (float)OrginalDataset[i, 1],
                    V2 = (float)OrginalDataset[i, 2],
                    V3 = (float)OrginalDataset[i, 3],
                    V4 = (float)OrginalDataset[i, 4],
                    V5 = (float)OrginalDataset[i, 5],
                    V6 = (float)OrginalDataset[i, 6],
                    V7 = (float)OrginalDataset[i, 7],
                    V8 = (float)OrginalDataset[i, 8],
                    V9 = (float)OrginalDataset[i, 9],
                    V10 = (float)OrginalDataset[i, 10],
                    V11 = (float)OrginalDataset[i, 11],
                    V12 = (float)OrginalDataset[i, 12]
                });
            }

        }
        public double CalculateAccuracy(double[,] Dataset)
        {
            IDataView TrainingSet = FormatData(Dataset);

            ITransformer model = CreateProtectedModel(TrainingSet);
            return EvaluateModel(model);
        }

        public static ITransformer CreateProtectedModel(IDataView TrainingSet)
        {
            var DataProcessPipeline = mlContext.Transforms.Concatenate("Features", new[] {"V0", "V1", "V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11"});
            var trainer = mlContext.Regression.Trainers.Sdca(labelColumnName: "V12");
            var TrainingPipeline = DataProcessPipeline.Append(trainer);
            ITransformer model = TrainingPipeline.Fit(TrainingSet);
            return model;
        }

        public static IDataView FormatData(double[,] Dataset)
        {
            int TrainingSetRowLimit = (int)(Dataset.GetLength(0) * 0.75);

            List<NumericModelIn> Protected = new List<NumericModelIn>();

            for (int i = 0; i < TrainingSetRowLimit; i++)
            {

                Protected.Add(new NumericModelIn
                {
                    V0 = (float)Dataset[i, 0],
                    V1 = (float)Dataset[i, 1],
                    V2 = (float)Dataset[i, 2],
                    V3 = (float)Dataset[i, 3],
                    V4 = (float)Dataset[i, 4],
                    V5 = (float)Dataset[i, 5],
                    V6 = (float)Dataset[i, 6],
                    V7 = (float)Dataset[i, 7],
                    V8 = (float)Dataset[i, 8],
                    V9 = (float)Dataset[i, 9],
                    V10 = (float)Dataset[i, 10],
                    V11 = (float)Dataset[i, 11],
                    V12 = (float)Dataset[i, 12]
                });


            }
            IDataView ProtectedData = mlContext.Data.LoadFromEnumerable<NumericModelIn>(Protected);

            return ProtectedData;
        }


        public static double EvaluateModel(ITransformer Model)
        {
            var predEngine = mlContext.Model.CreatePredictionEngine<NumericModelIn, NumericModelOut>(Model);
            List<double> Predictions = new List<double>();
            List<double> ActualValues = new List<double>();

            for(int i = 0; i < TestSet.Count;i++)
            {
                NumericModelOut prediction = predEngine.Predict(TestSet[i]);
                Predictions.Add((double)prediction.V12);
                ActualValues.Add(TestSet[i].V12);
            }


            return calc.CalculateEuclideanDistance(Predictions, ActualValues);
        }

    }
}
