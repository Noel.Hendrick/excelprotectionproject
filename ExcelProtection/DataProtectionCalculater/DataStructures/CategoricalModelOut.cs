﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;

namespace ExcelProtection
{
    class CategoricalModelOut
    {
        [ColumnName("PredictedLabel")]
        public string V6 { get; set; }
    }
}
