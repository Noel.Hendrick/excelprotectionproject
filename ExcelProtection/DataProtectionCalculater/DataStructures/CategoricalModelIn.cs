﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;

namespace ExcelProtection
{
    public class CategoricalModelIn
    {
       
        public string V0 { get; set; }

        public string V1 { get; set; }

        public string V2 { get; set; }

        public string V3 { get; set; }

        public string V4 { get; set; }

        public string V5 { get; set; }

        public string V6 { get; set; }
    }
}
