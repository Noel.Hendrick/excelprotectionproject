﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;

namespace ExcelProtection
{
    public class NumericModelIn
    {
        public float V0 { get; set; }

        public float V1 { get; set; }

        public float V2 { get; set; }

        public float V3 { get; set; }

        public float V4 { get; set; }

        public float V5 { get; set; }

        public float V6 { get; set; }

        public float V7 { get; set; }

        public float V8 { get; set; }

        public float V9 { get; set; }

        public float V10 { get; set; }

        public float V11 { get; set; }

        public float V12 { get; set; }

    }
}
