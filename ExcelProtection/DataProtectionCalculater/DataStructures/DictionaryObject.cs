﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class DictionaryObject
    {
        public DictionaryObject(Dictionary<string, int> _OrginalDictionary, Dictionary<string, int> _ProtectedDictionary, List<String> _VaribleNameList)
        {
            OrginalDictionary = _OrginalDictionary;

            ProtectedDictionary = _ProtectedDictionary;

            VaribleNameList = _VaribleNameList;
        }

        public Dictionary<string, int> OrginalDictionary { get; set; }

        public Dictionary<string, int> ProtectedDictionary { get; set; }

        public List<String> VaribleNameList { get; set; }
    }
}
