﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;

namespace ExcelProtection
{
    class NumericModelOut
    {
        [ColumnName("Score")]
        public float V12 { get; set; }
    }
}
