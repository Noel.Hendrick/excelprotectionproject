﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExcelProtection
{
    // an object for mean and standard deviation
    public class VaribleAnalytics
    {
        public double Mean { get; set; }
        public double StandardDeviation { get; set; }

    }
}
