﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelProtection
{
    public class CategoricalProtectionCalculater
    {
        public static Calculator calculator = new Calculator();
        ExcelHandler excelHandler = new ExcelHandler();


        public Tuple<double,double,double> CalculateQualityOfData(string[,] Dataset)
        {
            // this gets the orginal Dataset, calls the disclosure risk and information loss methods, then returns a Tuple with both doubles inside
            string[,] OrginalDataset = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);
            double DisclosureRisk = CalculateDisclosureRisk(Dataset, OrginalDataset);
            double InformationLoss = CalculateInformationLoss(Dataset, OrginalDataset);
            double result = (DisclosureRisk + InformationLoss) / 2;
            return Tuple.Create<double, double, double>(calculator.SetRoundedValue(result), DisclosureRisk, InformationLoss);



        }


        public static double CalculateDisclosureRisk(string[,] ProtectedDataset, string[,] OrginalDataset)
        {
            // takes in the new dataset and the orginal dataset, it measusres the disclosure risk using the Distance based disclosure risk formula 
            double HighestCommon = 0;
            int MostCommonRowNumber = 0;
            int NumberOfDiscoveredRows = 0;
            double NumberOfRows = 0;
            for (int t = 0; t < ProtectedDataset.GetLength(0); t++)
            {

                List<string> OrginalRow = calculator.ConvertRowToListCategoricalData(OrginalDataset, t);
                HighestCommon = 0;

                for (int i = 0; i < ProtectedDataset.GetLength(0); i++)
                {

                    List<string> ProtectedRow = calculator.ConvertRowToListCategoricalData(ProtectedDataset, i);
                    double CurrentCommonCounter = calculator.FindCommonElements(OrginalRow, ProtectedRow);

                    if (CurrentCommonCounter > HighestCommon)
                    {
                        HighestCommon = CurrentCommonCounter;
                        MostCommonRowNumber = i;
                    }


                }
                if (MostCommonRowNumber == t)
                {
                    NumberOfDiscoveredRows++;
                }

                NumberOfRows++;

            }

            double disclosureRisk = ((double)NumberOfDiscoveredRows / NumberOfRows);

            disclosureRisk = calculator.SetRoundedValue(disclosureRisk);

            return disclosureRisk;
        }


        public double CalculateInformationLoss(string[,] ProtectedDataset, string[,] OrginalDataset)
        {
            int NumberOfSamples = 3;
            double SumOfSamples = 0;
            for(int i = 0; i < NumberOfSamples; i++ )
            {
                SumOfSamples = SumOfSamples + CalculateContingencyTableLoss(ProtectedDataset, OrginalDataset);
            }

            return SumOfSamples / (double)NumberOfSamples;
        }


        public double CalculateContingencyTableLoss(string[,] ProtectedDataset, string[,] OrginalDataset)
        {
            var result = GetSampleFromDataset(OrginalDataset, ProtectedDataset);
            string[,] SampleOrginalDataset = result.Item1;
            string[,] SampleProtectedDataset = result.Item2;
            var ContingencyTables = CalculateContingencyTable(SampleOrginalDataset, SampleProtectedDataset);
            List<List<int>> OrginalContingencyTable = ContingencyTables.Item1;
            List<List<int>> ProtectedContingencyTable = ContingencyTables.Item2;
            double SumOfDifferences = 0;

            for(int i = 0; i < OrginalContingencyTable.Count(); i++ )
            {
                for (int j = 0; j < OrginalContingencyTable[i].Count(); j++)
                {
                    double temp = OrginalContingencyTable[i][j] - ProtectedContingencyTable[i][j];
                    temp = Math.Pow(temp, 2);
                    temp = Math.Sqrt(temp);
                    SumOfDifferences = SumOfDifferences + temp;
                  
                }
            }

            return calculator.SetRoundedValue(SumOfDifferences/OrginalDataset.Length);
        }


        public Tuple<List<List<int>>, List<List<int>>> CalculateContingencyTable(string[,]OrginalDataset , string[,] ProtectedDataset)
        {
            
            List<DictionaryObject> DictionaryList = new List<DictionaryObject>();

            for(int i = 0; i<OrginalDataset.GetLength(1); i++)
            {
                DictionaryList.Add(CalculateDictionaryRow(ProtectedDataset, OrginalDataset, i));
            }

            var ContingencyTables = CreateContingencyTable(DictionaryList);

            return ContingencyTables;
        }


        public Tuple<string[,] , string[,]> GetSampleFromDataset(string[,] OrginalDataset, string[,] ProtectedDataset)
        {
            Random ran = new Random();
            string[,] OrignalSampleDataset = new string[300,OrginalDataset.GetLength(1)];
            string[,] ProtectedSampleDataset = new string[300, OrginalDataset.GetLength(1)];

            int SampleRowIndex = ran.Next(0, (OrginalDataset.GetLength(0)-300));
            int iterator = 0;
            for(int i = SampleRowIndex; i<SampleRowIndex+300;i++)
            {
                for(int j = 0; j < OrginalDataset.GetLength(1);j++)
                {
                    OrignalSampleDataset[iterator, j] = OrginalDataset[i, j];
                    ProtectedSampleDataset[iterator, j] = ProtectedDataset[i, j];
                }

                iterator++;
            }

            return Tuple.Create<string[,], string[,]>(OrignalSampleDataset, ProtectedSampleDataset);
        }


        public DictionaryObject CalculateDictionaryRow(string[,] ProtectedDataset , string[,] OrginalDataset, int ColumnIndex)
        {

            Dictionary<string, int> OrginalColumnDictionary = new Dictionary<string, int>();
            Dictionary<string, int> ProtectedColumnDictionary = new Dictionary<string, int>();
            List<string> VaribleNameList = new List<string>();

            for (int RowIndex = 0; RowIndex < OrginalDataset.GetLength(0); RowIndex++)
            {
                if (OrginalColumnDictionary.ContainsKey(OrginalDataset[RowIndex, ColumnIndex]))
                {
                    OrginalColumnDictionary[OrginalDataset[RowIndex, ColumnIndex]]++;
                }

                if (ProtectedColumnDictionary.ContainsKey(ProtectedDataset[RowIndex, ColumnIndex]))
                {
                    ProtectedColumnDictionary[ProtectedDataset[RowIndex, ColumnIndex]]++;
                }

                if ((OrginalColumnDictionary.ContainsKey(OrginalDataset[RowIndex, ColumnIndex]) == false))
                {
                    OrginalColumnDictionary.Add(OrginalDataset[RowIndex, ColumnIndex], 1);
                    if (VaribleNameList.Contains(OrginalDataset[RowIndex, ColumnIndex]) == false)
                    {
                        VaribleNameList.Add(OrginalDataset[RowIndex, ColumnIndex]);
                    }
                }


                if ((ProtectedColumnDictionary.ContainsKey(ProtectedDataset[RowIndex, ColumnIndex]) == false))
                {
                    ProtectedColumnDictionary.Add(ProtectedDataset[RowIndex, ColumnIndex], 1);
                    if (VaribleNameList.Contains(ProtectedDataset[RowIndex, ColumnIndex]) == false)
                    {
                        VaribleNameList.Add(ProtectedDataset[RowIndex, ColumnIndex]);
                    }
                }
            }

            DictionaryObject ReturnInstance = new DictionaryObject(OrginalColumnDictionary, ProtectedColumnDictionary, VaribleNameList);
            return ReturnInstance;
        }


        public Tuple<List<List<int>>, List<List<int>>> CreateContingencyTable(List<DictionaryObject> DictionaryList)
        {
            List<List<int>> OrginalContingencyTable = new List<List<int>>();
            List<List<int>> ProtectedContingencyTable = new List<List<int>>();
            foreach (DictionaryObject instance in DictionaryList)
            {
                List<int> OrginalList = new List<int>();
                List<int> ProtectedList = new List<int>();
                for (int Index = 0; Index < instance.VaribleNameList.Count(); Index++)
                {
                    if (instance.OrginalDictionary.ContainsKey(instance.VaribleNameList[Index]) == false)
                    {
                        OrginalList.Add(0);
                    }

                    if (instance.ProtectedDictionary.ContainsKey(instance.VaribleNameList[Index]) == false)
                    {
                        ProtectedList.Add(0);
                    }

                    if (instance.OrginalDictionary.ContainsKey(instance.VaribleNameList[Index]) == true)
                    {
                        OrginalList.Add(instance.OrginalDictionary[instance.VaribleNameList[Index]]);
                    }

                    if (instance.ProtectedDictionary.ContainsKey(instance.VaribleNameList[Index]) == true)
                    {
                        ProtectedList.Add(instance.ProtectedDictionary[instance.VaribleNameList[Index]]);
                    }
                }

                OrginalContingencyTable.Add(OrginalList);
                ProtectedContingencyTable.Add(ProtectedList);
            }
           
            return Tuple.Create<List<List<int>>, List<List<int>>>(OrginalContingencyTable, ProtectedContingencyTable);
        }



    }
}
