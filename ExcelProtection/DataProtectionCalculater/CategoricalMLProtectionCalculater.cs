﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML;

namespace ExcelProtection
{
    public class CategoricalMLProtectionCalculater
    {
        public static MLContext mlContext = new MLContext();
        public static List<CategoricalModelIn> TestSet = new List<CategoricalModelIn>();
        public static Calculator calc = new Calculator();

        public CategoricalMLProtectionCalculater(string[,] Dataset)
        {
            ExcelHandler excelHandler = new ExcelHandler();
            string[,] OrginalDataset = Dataset;
            int TrainingSetRowLimit = (int)(OrginalDataset.GetLength(0) * 0.75);
            for (int i = TrainingSetRowLimit; i < OrginalDataset.GetLength(0); i++)
            {
                TestSet.Add(new CategoricalModelIn
                {
                    V0 = OrginalDataset[i, 0],
                    V1 = OrginalDataset[i, 1],
                    V2 = OrginalDataset[i, 2],
                    V3 = OrginalDataset[i, 3],
                    V4 = OrginalDataset[i, 4],
                    V5 = OrginalDataset[i, 5],
                    V6 = OrginalDataset[i, 6],
                });
            }
        }

        public double CalculateAccuracy(string[,] Dataset)
        {
            IDataView TrainingSet = FormatData(Dataset);

            ITransformer model = CreateProtectedModel(TrainingSet);
            return EvaluateModel(model, TestSet);
        }

        public static ITransformer CreateProtectedModel(IDataView TrainingSet)
        {

            var pipeline = mlContext.Transforms.Conversion.MapValueToKey(inputColumnName: "V6", outputColumnName: "Label")
                .Append(mlContext.Transforms.Text.FeaturizeText(inputColumnName: "V0", outputColumnName: "v0"))
                .Append(mlContext.Transforms.Text.FeaturizeText(inputColumnName: "V1", outputColumnName: "v1"))
                .Append(mlContext.Transforms.Text.FeaturizeText(inputColumnName: "V2", outputColumnName: "v2"))
                .Append(mlContext.Transforms.Text.FeaturizeText(inputColumnName: "V3", outputColumnName: "v3"))
                .Append(mlContext.Transforms.Text.FeaturizeText(inputColumnName: "V4", outputColumnName: "v4"))
                .Append(mlContext.Transforms.Text.FeaturizeText(inputColumnName: "V5", outputColumnName: "v5"))
                .Append(mlContext.Transforms.Concatenate("Features", "v0", "v1", "v2", "v3", "v4", "v5"));

            var trainingPipeline = pipeline.Append(mlContext.MulticlassClassification.Trainers.SdcaMaximumEntropy("Label", "Features"))
                .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

            ITransformer model = trainingPipeline.Fit(TrainingSet);
            return model;
        }

        public static IDataView FormatData(string[,] Dataset)
        {
            int TrainingSetRowLimit = (int)(Dataset.GetLength(0) * 0.80);
            List<CategoricalModelIn> Protected = new List<CategoricalModelIn>();

            for (int i = 0; i < TrainingSetRowLimit; i++)
            {

                Protected.Add(new CategoricalModelIn
                {
                    V0 = Dataset[i, 0],
                    V1 = Dataset[i, 1],
                    V2 = Dataset[i, 2],
                    V3 = Dataset[i, 3],
                    V4 = Dataset[i, 4],
                    V5 = Dataset[i, 5],
                    V6 = Dataset[i, 6]
                });


            }
            IDataView ProtectedData = mlContext.Data.LoadFromEnumerable<CategoricalModelIn>(Protected);

            return ProtectedData;
        }


        public static double EvaluateModel(ITransformer Model, List<CategoricalModelIn> Testset)
        {
            var predEngine = mlContext.Model.CreatePredictionEngine<CategoricalModelIn, CategoricalModelOut>(Model);
            List<string> Predictions = new List<string>();
            List<string> ActualValues = new List<string>();

            for (int i = 0; i < Testset.Count; i++)
            {
                CategoricalModelOut prediction = predEngine.Predict(Testset[i]);
                Predictions.Add(prediction.V6);
                ActualValues.Add(Testset[i].V6);
            }

            Calculator calc = new Calculator();
            double x = calc.FindCommonElements(Predictions, ActualValues);
            x = x /Predictions.Count;
            x = calc.SetRoundedValue(x);
            return x;
        }
    }
}
