﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;

namespace ExcelProtection
{
    public class UIObject
    {
        public PointPairList PointsForPopulation { get; set; }
        public double MaxFitness { get; set; }
        public double AverageFitness { get; set; }
        public int GenerationNumber { get; set; }
        public bool UseStatistic { get; set; }
    }
}
