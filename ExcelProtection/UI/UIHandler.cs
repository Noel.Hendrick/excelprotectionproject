﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace ExcelProtection
{
    public partial class UIHandler : Form
    {
        GraphPane pane;
        TextBox Stats;
        System.Windows.Forms.Label heading;
        UIObject Transfer;
        ColorSymbolRotator csr = new ColorSymbolRotator();

        public UIHandler(UIObject _transfer)
        {
            InitializeComponent();

            Transfer = _transfer;
            pane = zedGraphControl1.GraphPane;
            Stats = textBox1;
            heading = label1;

            GraphMaker();
            CreateTextBox();
            LabelText();

        }


        public void GraphMaker()
        {
            //generate pane
            pane.Title.Text = "Genetic Proccess";
            pane.XAxis.Title.Text = "Information Loss";
            pane.YAxis.Title.Text = "Disclosure Risk";



        }

        public void CreateTextBox()
        {
            
            Stats.ReadOnly = true;
            Stats.TextAlign = HorizontalAlignment.Center;
            Stats.ScrollBars = ScrollBars.Vertical;
        }

        private void LabelText()
        {
            heading.Text = "Generation Analytics";
        }



        public void UpdateUI(UIObject _Transfer)
        {
            Transfer = _Transfer;
            LineItem pointsCurve;

            Stats.Invoke((MethodInvoker)delegate {
                Stats.AppendText("Generation: " + Transfer.GenerationNumber + "\u000D\u000A");
                Stats.AppendText("The average population fitness is: " + Transfer.AverageFitness + "\u000D\u000A");
                Stats.AppendText("The fittest chromosome in the population is: " + Transfer.MaxFitness + "\u000D\u000A");
                Stats.AppendText("*------------------------------------------------------------------------------* \u000D\u000A");

                pointsCurve = pane.AddCurve("Generation: " + Transfer.GenerationNumber, Transfer.PointsForPopulation, csr.NextColor);
                pointsCurve.Line.IsVisible = false;
                zedGraphControl1.AxisChange();
                zedGraphControl1.Refresh();
               

            });
            
            

            


        }
    }
}
