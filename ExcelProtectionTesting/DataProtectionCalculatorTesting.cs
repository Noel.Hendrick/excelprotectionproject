﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OfficeOpenXml;
using ExcelProtection;

namespace ExcelProtectionTesting
{
    class DataProtectionCalculatorTesting
    {
        [Test]
        public void CalculateNumericDisclosureRiskTest()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CalculaterTestSheet.xlsx", "Sheet1", 1, 1);
            double[,] DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\DisclosureRiskExcelsheetTest.xlsx", "Sheet1", 1, 1);
            double disclosureRisk = NumericProtectionCalculater.CalculateDisclosureRisk(Dataset, DatasetChecker);
            Assert.AreEqual(90, disclosureRisk);
        }

        [Test]
        public void CalculateNumericInformationLossTest()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CalculaterTestSheet.xlsx", "Sheet1", 1, 1);
            double[,] DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\DisclosureRiskExcelsheetTest.xlsx", "Sheet1", 1, 1);
            NumericProtectionCalculater protectionCalc = new NumericProtectionCalculater();
            double InformationLoss = protectionCalc.CalculateInformationLoss(Dataset, DatasetChecker);
            Assert.AreEqual(0.35882132024315, InformationLoss);
        }

        [Test]
        public void CalculateNumericDisclosureRiskTest2()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\StandardData.xlsx", "EIA", 2, 4);
            double[,] OrginalDataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\StandardData.xlsx", "EIA", 2, 4);
            double disclosureRisk = NumericProtectionCalculater.CalculateDisclosureRisk(Dataset, OrginalDataset);
            Assert.AreEqual(100, disclosureRisk);
        }

        [Test]
        public void CalculateNumericInformationLossTest2()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\StandardData.xlsx", "EIA", 2, 4);
            double[,] OrginalDataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\StandardData.xlsx", "EIA", 2, 4);
            NumericProtectionCalculater protectionCalc = new NumericProtectionCalculater();
            double InformationLoss = protectionCalc.CalculateInformationLoss(Dataset, OrginalDataset);
            Assert.AreEqual(0, InformationLoss);
        }



        [Test]
        public void CalculateCategoricalInformationLossTest()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            string[,] Dataset = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CategoricalTestSheet1.xlsx", "Sheet1", 1, 1);
            string[,] DatasetChecker = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CategoricalTestSheet2.xlsx", "Sheet1", 1, 1);
            CategoricalProtectionCalculater protectionCalc = new CategoricalProtectionCalculater();
            double InformationLoss = protectionCalc.CalculateContingencyTableLoss(Dataset, DatasetChecker);
            Assert.AreEqual(62.5, InformationLoss);
        }



        [Test]
        public void CalculateCategoricalDisclosureRiskTest()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            string[,] Dataset = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CategoricalTestSheet3.xlsx", "Sheet1", 1, 1);
            string[,] DatasetChecker = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CategoricalTestSheet4.xlsx", "Sheet1", 1, 1);
            CategoricalProtectionCalculater protectionCalc = new CategoricalProtectionCalculater();
            double DisclosureRisk = CategoricalProtectionCalculater.CalculateDisclosureRisk(Dataset, DatasetChecker);
            Assert.AreEqual(80, DisclosureRisk);
        }

        [Test]
        public void NumericMLProtectionCalculater()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx", "Census", 2, 1);
            NumericMLProtectionCalculater ml = new NumericMLProtectionCalculater(Dataset);
            double Error = ml.CalculateAccuracy(Dataset);
            Assert.AreEqual(0, 0);
        }

        [Test]
        public void CategoricalMLProtectionCalculater()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            string[,] Dataset = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);
            CategoricalMLProtectionCalculater ml = new CategoricalMLProtectionCalculater(Dataset);
            double Error = ml.CalculateAccuracy(Dataset);
            Assert.AreEqual(0, 0);
        }

        [Test]
        public void CalculateWeightedNumericDisclosureRiskTest()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CalculaterTestSheet.xlsx", "Sheet1", 1, 1);
            double[,] DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\DisclosureRiskExcelsheetTest.xlsx", "Sheet1", 1, 1);
            double disclosureRisk = DistanceGenetic.StartSimulation(Dataset, DatasetChecker);
            Assert.AreEqual(90, disclosureRisk);
        }

    }
}
