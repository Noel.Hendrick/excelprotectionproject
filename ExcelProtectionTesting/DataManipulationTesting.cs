﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ExcelProtection;
using OfficeOpenXml;
using System.IO;
using System.Windows.Forms;
using ZedGraph;

namespace ExcelProtectionTesting
{
    class DataManipulationTesting
    {
        [Test]
        public void DataStandardizerTest()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CalculaterTestSheet.xlsx","Sheet1",2,4);
            double[,] DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\DataStandardizerTestSheet.xlsx", "Sheet1", 2, 4);
            DataStandardizer standardizer = new DataStandardizer();
            Dataset = standardizer.DataStandardizing(Dataset);
            Assert.AreEqual(Dataset, DatasetChecker);
        }

        [Test]
        public void NoiseAdditionTest()
        {
            PointPairList list1 = new PointPairList();
            for (int i = 1; i < 100; i = i + 3)
            {
                ExcelHandler excelHandler = new ExcelHandler();
                double[,] DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx", "Census", 2, 1);
                NoiseAddition noise = new NoiseAddition();
                double[,] Dataset = NoiseAddition.AddNoiseAdditon(DatasetChecker, i);
                DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx", "Census", 2, 1);
                NumericProtectionCalculater protectionCalc = new NumericProtectionCalculater();
                double InformationLoss = protectionCalc.CalculateInformationLoss(Dataset, DatasetChecker);
                double DisclosureRisk = NumericProtectionCalculater.CalculateDisclosureRisk(Dataset, DatasetChecker);
                list1.Add(DisclosureRisk, InformationLoss);

            }
        }

        [Test]
        public void MicroaggregationTest()
        {
            PointPairList list1 = new PointPairList();
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx", "Census", 2, 1);

            for (int i = 1; i < DatasetChecker.GetLength(0); i = i + 1)
            {
                Microaggregation micro = new Microaggregation(DatasetChecker, i);
                double[,] Dataset = micro.Microaggregator();
                NumericProtectionCalculater protectionCalc = new NumericProtectionCalculater();
                double InformationLoss = protectionCalc.CalculateInformationLoss(Dataset, DatasetChecker);
                double DisclosureRisk = NumericProtectionCalculater.CalculateDisclosureRisk(Dataset, DatasetChecker);
                list1.Add(DisclosureRisk, InformationLoss);
            }
        }

        [Test]
        public void RankSwappingTest()
        {
            PointPairList list1 = new PointPairList();
            for (int i = 1; i < 100; i = i+3)
            {
                ExcelHandler excelHandler = new ExcelHandler();
                double[,] DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx", "Census", 2, 1);
                RankSwapping rank = new RankSwapping();
                double[,] Dataset = rank.RankSwapper(DatasetChecker, i);
                DatasetChecker = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx", "Census", 2, 1);
                NumericProtectionCalculater protectionCalc = new NumericProtectionCalculater();
                double InformationLoss = protectionCalc.CalculateInformationLoss(Dataset, DatasetChecker);
                double DisclosureRisk = NumericProtectionCalculater.CalculateDisclosureRisk(Dataset, DatasetChecker);
                list1.Add(DisclosureRisk, InformationLoss);
            }

        }

        [Test]
        public void PRAMTesting()
        {
            PointPairList list1 = new PointPairList();
            for (double i = 0.01; i < 1; i = i + 0.01)
            {
                ExcelHandler excelHandler = new ExcelHandler();
                string[,] DatasetChecker = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);
                PRAM pram = new PRAM();
                string[,] Dataset = pram.ReturnPRAM(DatasetChecker, i);
                DatasetChecker = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);
                CategoricalProtectionCalculater protectionCalc = new CategoricalProtectionCalculater();
                double InformationLoss = protectionCalc.CalculateInformationLoss(Dataset, DatasetChecker);
                double DisclosureRisk = CategoricalProtectionCalculater.CalculateDisclosureRisk(Dataset, DatasetChecker);
                list1.Add(DisclosureRisk, InformationLoss);
            }
        }

        [Test]
        public void CategoricalMicroaggregationTesting()
        {
            PointPairList list1 = new PointPairList();
            ExcelHandler excelHandler = new ExcelHandler();
            string[,] DatasetChecker = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);

            for (int i = 1; i < DatasetChecker.GetLength(0); i = i+1)
            {
                DatasetChecker = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);
                CategoricalMicroaggregation micro = new CategoricalMicroaggregation(DatasetChecker, i);
                string[,] Dataset = micro.Microaggregator();
                DatasetChecker = excelHandler.GetCategoricalExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\carDataset.xlsx", "car", 1, 1);
                CategoricalProtectionCalculater protectionCalc = new CategoricalProtectionCalculater();
                double InformationLoss = protectionCalc.CalculateInformationLoss(Dataset, DatasetChecker);
                double DisclosureRisk = CategoricalProtectionCalculater.CalculateDisclosureRisk(Dataset, DatasetChecker);
                list1.Add(DisclosureRisk, InformationLoss);
            }
        }
    }
}
