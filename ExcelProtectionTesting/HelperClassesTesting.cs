﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ExcelProtection;
using OfficeOpenXml;
using System.IO;

namespace ExcelProtectionTesting
{
    public class HelperClassesTesting
    {
        [Test]
        public void CalculateMeanTest()
        {
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>() { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
            double result = calculator.CalculateMean(TestList);
            Assert.AreEqual(4.5000000000, result);

        }

        [Test]
        public void CalculateStandardDeviationTest()
        {
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>() { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
            double result = calculator.CalculateStandardDeviation(TestList);
            Assert.AreEqual(2.87228132326901, result);

        }

        [Test]
        public void ReturnStandardDeviationAndMeanTest()
        {
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>() { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
            var result = calculator.ReturnStandardDeviationAndMean(TestList);
            Assert.AreEqual(4.5000000000, result.Item1);
            Assert.AreEqual(2.87228132326901, result.Item2);
        }


        [Test]
        public void ConvertToListTest()
        {
            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CalculaterTestSheet.xlsx","Sheet1",2,4);
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>();
            for(int i = 2; i<=10;i++)
            {
                for (int j = 4; j <= 10; j++)
                {
                    TestList.Add(i);
                }
            }

            List<double> DataList = calculator.ConvertToList(Dataset);
            Assert.AreEqual(TestList, DataList);

        }

        [Test]
        public void ConvertColumnToListTest()
        {

            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CalculaterTestSheet.xlsx", "Sheet1", 2, 4);
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>();
            for (int i = 2; i <= 10; i++)
            {

                    TestList.Add(i);
                
            }

            List<double> DataList = calculator.ConvertColumnToList(Dataset,0);
            Assert.AreEqual(TestList, DataList);

        }

        [Test]
        public void setRoundedValueTest()
        {
            Calculator calculator = new Calculator();
            double ShortDoubleTest = calculator.SetRoundedValue(0.6666);
            double ExactDoubleTest = calculator.SetRoundedValue(0.66666666666666);
            double LongDoubleTest = calculator.SetRoundedValue(0.666666666666666666666);

            Assert.AreEqual(0.6666,ShortDoubleTest);
            Assert.AreEqual(0.66666666666666, ExactDoubleTest);
            Assert.AreEqual(0.66666666666667, LongDoubleTest);


        }

        [Test]
        public void CalculateEuclideanDistanceTest()
        {
           
            Calculator calculator = new Calculator();
            List<double> DataList = new List<double> { 2, 4, 3, 6, 7, 2, 6 };
            List<double> DataList2 = new List<double> { 5, 9, 2, 8, 3, 0, 1 };
            double distance = calculator.CalculateEuclideanDistance(DataList, DataList2);
            Assert.AreEqual(9.16515138991168, distance);

        }

        [Test]
        public void ConvertRowToListTest()
        {

            ExcelHandler excelHandler = new ExcelHandler();
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CalculaterTestSheet.xlsx", "Sheet1", 2, 4);
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>();
            for (int i = 4; i <= 10; i++)
            {

                TestList.Add(2);

            }

            List<double> DataList = calculator.ConvertRowToList(Dataset, 0);
            Assert.AreEqual(TestList, DataList);

        }

        [Test]
        public void CalculateMeanSquareErrorTest()
        {
            Calculator calculator = new Calculator();
            double[,] DataList = new double[,] { { 1, 2, 3 }, { 1, 2, 3} };
            double[,] DataList2 = new double[,] { { 3, 2, 1 }, { 3, 2, 1 } };
            double Error = calculator.CalculateMeanSquareError(DataList, DataList2);
            Assert.AreEqual(2.66666666666667, Error);

        }

        [Test]
        public void CalculateVarienceTest()
        {
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>() { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
            double result = calculator.CalculateVarience(TestList);
            Assert.AreEqual(8.25, result);

        }


        [Test]
        public void ReturnStandardDeviationAndMeanAndVarienceTest()
        {
            Calculator calculator = new Calculator();
            List<double> TestList = new List<double>() { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
            var result = calculator.ReturnStandardDeviationAndMeanAndVarience(TestList);
            Assert.AreEqual(4.5000000000, result.Item1);
            Assert.AreEqual(2.87228132326901, result.Item2);
            Assert.AreEqual(8.25, result.Item3);
        }


    }
}
